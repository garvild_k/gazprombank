import React, { Component } from "react";
import Heatmap from "./Heatmap";
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import Point from "./point"
import L from 'leaflet';

class MainMap extends Component {
  constructor(props) {
      super(props);
      this.state = {
        data: [],
        loaded: false,
        placeholder: "Loading",
        zoom: 13,
    };
  };

  componentDidMount() {
    fetch("/api/v1/map/")
        .then(response => {
            if (response.status > 400) {
                return this.setState(() => {
                    return {placeholder: "Something went wrong!",};
                });
            }
            return response.json();
        })
        .then(data => {
            this.setState(() => {
              return {
                  data,
                  loaded: true,
                };
            });
        });
}
coordMap() {
  const pointerIcon = new L.Icon({
    iconUrl: "/static/images/atm.svg",
    iconRetinaUrl: "/static/images/atm.svg",
    iconAnchor: [5, 55],
    popupAnchor: [10, -44],
    iconSize: [15, 35],
})
return(
  <div>
      {this.state.data.map(coord => {
    const position = [coord.geolocationlatitude, coord.geolocationlongitude]
    return(
      <Marker position={position} icon={pointerIcon}>
        <Popup key={coord.id}>
          {coord.addressfulladdress}
        </Popup>
      </Marker>
      )
    }
  )
}
  </div>
)
}



coordMapCreatePlacemark() {
  const center = [55.753215, 37.622504]
  return (
          <Map center={center} zoom={12} maxZoom={18} >
              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Point/>
              {this.coordMap()}
              <Heatmap/>
          </Map>
  )
}


  render() {
    return this.coordMapCreatePlacemark()
}
}

export default MainMap;