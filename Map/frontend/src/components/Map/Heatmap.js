import React, { Component } from "react";
import HeatmapLayer from 'react-leaflet-heatmap-layer';

class HeatmapTest extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          loaded: false,
          placeholder: "Loading",
          zoom: 13,
      };
    };
  
    componentDidMount() {
      fetch("/api/v1/moscow/")
          .then(response => {
              if (response.status > 400) {
                  return this.setState(() => {
                      return {placeholder: "Something went wrong!",};
                  });
              }
              return response.json();
          })
          .then(data => {
              this.setState(() => {
                return {
                    data,
                    loaded: true,
                  };
              });
          });
  }
  coordMap() {
    {this.state.data.map(coord => {
      const position = [coord.lat, coord.long, coord.living_house]
        return(
          position
        )
      }
    )
  }
  }
  
  coordMapCreatePlacemark() {
    const center = [55.753215, 37.622504]
    return (
                <HeatmapLayer
                    fitBoundsOnLoad
                    fitBoundsOnUpdate
                    radius={30}
                    points={this.state.data.map(coord => {
                      const position = [coord.lat, coord.long, coord.living_house === 'Нет данных' ? 131 : coord.living_house * 3]
                        return(
                          position
                        )
                      }
                    )
                  }
                    longitudeExtractor={m => m[1]}
                    latitudeExtractor={m => m[0]}
                    intensityExtractor={m => parseFloat(m[2])} 
                />
    )
  }
  
    render() {
      return this.coordMapCreatePlacemark()
  }
  }
  
  export default HeatmapTest;
