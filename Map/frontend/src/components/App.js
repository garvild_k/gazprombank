import React from 'react';
import Header from "./Header/Header";
import Map from "./Map/Map";
import InfoBlock from "./InfoBlock/InfoBlock";

const App = () => {
    return (
        <div className={"grid"}>
            <Header/>
            <Map/>
            <InfoBlock/>
        </div>
    )
}
export default App;