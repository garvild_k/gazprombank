from .models import Atm, mosHouse, Datapoint
from rest_framework import serializers


class MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Atm
        fields = ('__all__')


class MoscowSerializer(serializers.ModelSerializer):
    class Meta:
        model = mosHouse
        fields = ('__all__')


class PointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Datapoint
        fields = ('__all__')