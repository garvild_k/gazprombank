from django.shortcuts import render
from rest_framework import generics
from .models import Atm, mosHouse, Datapoint
from .serializers import MapSerializer, MoscowSerializer, PointSerializer
from rest_framework.response import Response


class MapList(generics.ListAPIView):
    queryset = Atm.objects.using('DATA').all().filter(addressregion='Москва')
    serializer_class = MapSerializer


class MoscowList(generics.ListAPIView):
    queryset = mosHouse.objects.using('DATA').all()
    serializer_class = MoscowSerializer


class PointList(generics.ListAPIView):
    queryset = Datapoint.objects.using('DATA').all()
    serializer_class = PointSerializer
